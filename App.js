import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image } from 'react-native';
import LoginForm from './src/components/LoginForm';

export default class App extends Component {
  render() {
    const { container, logoContainer, logoImg, loginFormContainer } = styles;

    return (
      <View style={ container }>
        <View style={ logoContainer }>
          <Image
            source={ require('./src/images/logo_wd.png') }
            resizeMode={ 'center' }
            style={ logoImg }
          />
        </View>
        <View style={ loginFormContainer }>
          <LoginForm />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginLeft: 8,
    marginTop: 8,
    justifyContent: 'center',
    alignItems: 'flex-start',
    backgroundColor: '#f5f5f5',
    flexDirection: 'column',
  },
  logoContainer: {
    alignItems: 'center',
    alignSelf: 'center',
  },
  logoImg: {
    width: 200,
    height: 200,
  },
  loginFormContainer: {
    alignItems: 'flex-start',
  },
});
