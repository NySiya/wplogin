import React, { Component } from 'react';
import {
  View, Text, StyleSheet, TextInput, Dimensions,
} from 'react-native';
import { CheckBox, Button } from 'react-native-elements';

export default class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: 'sed2',
      pwd: 'se',
      checked: false,
    };
  }

  _handleChangeUsername = (name) => {
    this.state.username = name;
  }

  _handleChangePassword = (password) => {
    this.state.pwd = password;
  }

  render () {
    const {
      container, formContainer, forgetPwd, userInputCont, pwdInputCont, label, input, checkboxStyle, submitStyle, submitSection,
    } = styles;

    return (
      <View style={ container }>
        <View style={ formContainer }>
          <View style={ userInputCont }>
            <Text style={ label }>Username or Email</Text>
            <TextInput
              onChangeText={(text) => this._handleChangeUsername(text) }
              value={ this.state.username }
              style={ input }
            />
          </View>
          <View style={ pwdInputCont }>
            <Text style={ label }>Password</Text>
            <TextInput
              secureTextEntry={ true }
              onChangeText={(text) => this._handleChangePassword(text)}
              value={ this.state.pwd }
              style={ input }
            />
          </View>
          <View style={ submitSection }>
            <CheckBox
               title='Remember Me'
               checkedColor='green'
               checked={ this.state.checked }
               onPress={() => this.setState({checked: !this.state.checked})}
               containerStyle={ checkboxStyle }
            />
            <Button
              title="Log In"
              size={15}
              buttonStyle={ submitStyle }
            />
          </View>
        </View>

        <View style={ forgetPwd }>
          <Text style={ label }>Lost your password?</Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    marginLeft: 8,
    marginRight: 16,
  },
  formContainer: {
    backgroundColor: '#ffffff',
    paddingLeft: 16,
    paddingRight: 16,
    paddingBottom: 24,
    width: Dimensions.get('window').width - 32,
  },
  forgetPwd: {
    marginTop: 24,
    marginLeft: 16,
  },
  userInputCont: {
    flexDirection: 'column',
    paddingTop: 8,
  },
  pwdInputCont: {
    flexDirection: 'column',
    paddingTop: 16,
  },
  submitSection: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  label: {
    color: '#A1A5A9',
    paddingBottom: 4,
  },
  input: {
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    borderRadius: 3,
    backgroundColor: '#f5f5f5',
    height: 40,
  },
  checkboxStyle: {
    marginLeft: 0,
    paddingLeft: 0,
    backgroundColor: 'transparent',
    borderColor: 'transparent',
  },
  submitStyle: {
    alignItems: 'flex-end',
    // justifyContent: 'flex-end',
    alignSelf: 'flex-end',
  },
})
